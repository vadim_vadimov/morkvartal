<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ЖК "Морской Квартал" в Сочи - Официальный сайт застройщика</title>
	<meta name="description" content="ЖК Морской Квартал в Сочи - дом комфорт-класса в 7 минутах от моря. Бассейн, подземный паркинг. Напрямую от застройщика.">
	<meta name="keywords" content="Морской Квартал, Сочи, Застройщик, Вардане, Новостройка, ЖК, Дом">
	<link rel="stylesheet"  href="css/style.css">
	<link rel="icon" href="img/favicon.ico" type="image/x-icon">
	<!-- Facebook Pixel Code -->
	<script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '297698297598052');
        fbq('track', 'PageView_mkvartal');
	</script>
	<noscript><img height="1" width="1" style="display:none"
				   src="https://www.facebook.com/tr?id=297698297598052&ev=PageView_mkvartal&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
</head>
<body>
	<div class="parallax-content">
		<div class="intro-block" style="background-image: url(img/intro-bg.jpg);">
			<header class="header">
				<div class="container">
					<div class="header__content">
						<a href="index.html" class="header__logo"><img src="img/logo.png" alt="logo"></a>
						<div class="header__item">
							<div class="header__item-text">
								<strong class="header__big-item-title">«Морской квартал» —</strong>
								<p>позвольте себе квартиру <br>на берегу Черного моря</p>
							</div>
						</div>
					</div>
					<div class="header__list">
						<div class="header__item">
							<img src="img/header-el-1.png" alt="header-el-1">
							<div class="header__item-text">
								<p>Сочи, <strong class="header__item-title">поселок Вардане</strong> Львовская ул.</p>
							</div>
						</div>
						<div class="header__item">
							<img src="img/header-el-2.png" alt="header-el-2">
							<div class="header__item-text">
								<p>Сдача первого литера</p>
								<strong class="header__item-title">IV квартал 2019</strong>
							</div>
						</div>
					</div>
					<div class="header__tel-content">
						<a href="tel:79683002066" class="header__tel jsTrackContact">+7 968 300-20-66</a>
						<a data-fancybox data-src="#popup" href="javascript://" class="header__order-call">Заказать звонок</a>
					</div>
				</div>
			</header>

			<div class="intro">
				<div class="container">
					<div class="intro__content">
						<div class="intro__title-content">
							<h1><span class="title-up">Квартиры комфорт-класса</span> <span class="title-down">в 7 минутах от моря</span></h1>
							<p class="intro__title-description">Охраняемая территория,паркинг, бассейн</p>
							<a data-fancybox data-src="#popup-3" href="javascript://" class="btn-main  btn-main--col"><span>Получить доступные квартиры<br>и актуальные цены</span></a>
						</div>
						<div class="intro__mark">
							<div class="mark">
								<span>От</span>
								<p>61 000</p>
								<span>руб. за кв.м</span>
							</div>
						</div>
					</div>
					<div class="intro__description">
						<p><span class="mask-up">Охраняемая территория,</span><span class="mask-down">паркинг, бассейн</span></p>
					</div>
				</div>
			</div>

			<div class="parallax-wrap">
				<div class="parallax-item  layer" data-depth="0.06">
					<img src="img/sky-1-par.png" alt="sky-parallax">
				</div>
				<div class="parallax-item  layer" data-depth="0.12">
					<img src="img/sky-2-par.png" alt="sky-parallax">
				</div>
				<div class="parallax-item  layer" data-depth="0.3">
					<img src="img/bird-1.png" alt="bird">
				</div>
			</div>
		</div>

		<div class="opportunity">
			<div class="container">
				<div class="opportunity__list">
					<div class="opportunity__item">
						<div class="opportunity__image">
							<img src="img/opportunity-1.png" alt="opportunity">
						</div>
						<strong>ФЗ 215</strong>
						<p>Вы можете быть уверены в законности сделки. Нет принудительного страхования</p>
					</div>
					<div class="opportunity__item">
						<div class="opportunity__image">
							<img src="img/opportunity-2.png" alt="opportunity">
						</div>
						<strong>Возможность <br>прописки</strong>
						<p>Риски, связанные с правовым статусом объекта, исключены. В случае переезда в Сочи — с пропиской не возникнет проблем</p>
					</div>
					<div class="opportunity__item">
						<div class="opportunity__image">
							<img src="img/opportunity-3.png" alt="opportunity">
						</div>
						<strong>Зоны отдыха, <br>как в апарт-комплексах премиум класса</strong>
						<p>Подогреваемый бассейн, прогулочные дорожки, детская площадка, зоны барбекю</p>
					</div>
				</div>
				<div class="parallax-wrap">
					<div class="parallax-item  layer" data-depth="0.6">
						<img src="img/leaf-1.png" alt="leaf">
					</div>
					<div class="parallax-item  layer" data-depth="0.15">
						<img src="img/leaf-2.png" alt="leaf">
					</div>
					<div class="parallax-item  layer" data-depth="0.05">
						<img src="img/leaf-3.png" alt="leaf">
					</div>
				</div>
			</div>
		</div>

		<section class="presentation">
			<div class="container">
				<div class="presentation__content">
					<div class="main-title">
						<h2>Скачайте полную презентацию жилого комплекса «Морской квартал»</h2>
					</div>
					<div class="presentation__mark">
						<div class="mark">
							<p>16</p>
							<span>страниц</span>
						</div>
					</div>
				</div>
				<div class="main-form">
					<div class="main-form__wrapper">
						<strong class="main-form__title">Введите ваши данные</strong>
						<p class="main-form__subtitle">чтобы скачать презентацию <br>жилого комплекса в 1 клик</p>
						<form action="" class="mail-tel-form" novalidate="novalidate">
							<input type="tel" name="tel" placeholder="Введите ваш телефон">
							<input type="email" name="email" placeholder="Введите вашу почту">
							<button class="btn-main"><span>скачать презентацию</span></button>
							<input type="hidden" name="form_name" value="Скачать презентацию в один клик">
							<input type="hidden" name="utm" value="<?php echo htmlspecialchars(json_encode($_GET)); ?>">
						</form>
						<p class="main-form__description">Нажимая на кнопку, вы принимаете <br>условия <a href="#">передачи данных</a></p>
					</div>
				</div>
			</div>
			<div class="parallax-wrap">
				<div class="parallax-item  layer" data-depth="1">
					<img src="img/bird-2.png" alt="bird">
				</div>
				<div class="parallax-item  layer" data-depth="0.05">
					<img src="img/book.png" alt="book">
				</div>
			</div>
			<img class="presentation__tree" src="img/trees-1.png" alt="trees">
		</section>

		<section class="infrastructure" id="infrastruktura" style="background-image: url(img/infrastructure-bg.jpg);">
			<div class="container">
				<div class="infrastructure__content-wrap">
					<div class="infrastructure__content">
						<div class="main-title">
							<h2>Спокойный <span class="main-title__description">Поселок Вардане</span> курорт с развитой инфраструктурой</h2>
							<p>Где комфортно отдыхать</p>
						</div>
					</div>
					<div class="infrastructure__info">
						<p>В шаговой доступности есть <br>все, что необходимо для жизни</p>
						<div class="infrastructure__info-content">
							<div class="infrastructure__info-list">
								<strong>10 минут</strong>
								<ul>
									<li>Магазины</li>
									<li>Школа</li>
									<li>Детский сад</li>
								</ul>
							</div>
							<div class="infrastructure__info-list">
								<strong>5-10 минут</strong>
								<ul>
									<li>До остановок общественного транспорта</li>
									<li>Автобус</li>
									<li>Электричка</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="infrastructure__list">
					<div class="infrastructure__item">
						<div class="infrastructure__image">
							<img src="img/infrastructure-1.png" alt="infrastructure">
						</div>
						<strong>850 метров <br>до пляжа</strong>
						<span>или 10 минут медленным шагом</span>
						<ul>
							<li>Галечный пляж <br>регулярно убирают</li>
							<li>Море прозрачное и чистое</li>
							<li>Купальный сезон длится <br>6 месяцев — с мая по октябрь</li>
						</ul>
					</div>
					<div class="infrastructure__item">
						<div class="infrastructure__image">
							<img src="img/infrastructure-2.png" alt="infrastructure">
						</div>
						<strong>Зеленый, экологически чистый район</strong>
						<p>Субтропические <br>смешанные леса спускаются <br>к черноморскому побережью</p>
					</div>
				</div>
				<div class="infrastructure__info  infrastructure__info--mobile">
					<p>В шаговой доступности есть <br>все, что необходимо для жизни</p>
					<div class="infrastructure__info-content">
						<div class="infrastructure__info-list">
							<strong>10 минут</strong>
							<ul>
								<li>Магазины</li>
								<li>Школа</li>
								<li>Детский сад</li>
							</ul>
						</div>
						<div class="infrastructure__info-list">
							<strong>5-10 минут</strong>
							<ul>
								<li>До остановок общественного транспорта</li>
								<li>Автобус</li>
								<li>Электричка</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="parallax-wrap">
					<div class="parallax-item  layer" data-depth="1">
						<img src="img/bird-1.png" alt="bird">
					</div>
				</div>
			</div>
		</section>

		<section class="attraction" id="raspolozheniye" style="background-image: url(img/attraction-bg.jpg);">
			<div class="container">
				<div class="main-title">
					<h2>От вашей квартиры <br>рукой подать до основных достопримеча<wbr>тельностей</h2>
				</div>
				<div class="attraction__content">
					<div class="attraction__transports">
						<img src="img/road-line.png" alt="" class="attraction__road-line">
						<div class="attraction__tranport-item">
							<div class="attraction__tranport-wrap">
								<img src="img/car.svg" alt="car">
							</div>
							<p>Автомобиль</p>
						</div>
						<div class="attraction__tranport-item">
							<div class="attraction__tranport-wrap">
								<img src="img/train.svg" alt="train">
							</div>
							<p>Электричка</p>
						</div>
						<div class="attraction__tranport-item">
							<div class="attraction__tranport-wrap">
								<img src="img/bus.png" alt="bus">
							</div>
							<p>Автобус</p>
						</div>
					</div>
					<div class="attraction__list">
						<div class="attraction__item">
							<img class="attraction__main-img" src="img/attraction-1.jpg" alt="attraction-item">
							<h4 class="attraction__item-title">Лазаревское</h4>
							<p>Аквапарк, океанариум, Свирское ущелье</p>
							<span>37 км</span>
							<div class="attraction__item-panel">
								<span class="attraction__metres-mobile">37 км</span>
								<div class="attraction__divider"></div>
								<div class="attraction__item-icons">
									<img src="img/car.svg" alt="car">
									<img src="img/train.svg" alt="train">
									<img src="img/bus.png" alt="bus">
								</div>
							</div>
						</div>
						<div class="attraction__item">
							<img class="attraction__main-img" src="img/attraction-2.jpg" alt="attraction-item">
							<h4 class="attraction__item-title">Вардане</h4>
							<p>Чистое море,<br>живописные окрестности</p>
							<span>30 км</span>
							<div class="attraction__item-panel">
								<span class="attraction__metres-mobile">30 км</span>
								<div class="attraction__divider"></div>
								<div class="attraction__item-icons">
									<img src="img/car.svg" alt="car">
									<img src="img/train.svg" alt="train">
									<img src="img/bus.png" alt="bus">
								</div>
							</div>
						</div>
						<div class="attraction__item">
							<img class="attraction__main-img" src="img/attraction-3.jpg" alt="attraction-item">
							<h4 class="attraction__item-title">Сочи</h4>
							<p>Дендрарий, морской вокзал, парки</p>
							<span>60 км</span>
							<div class="attraction__item-panel">
								<span class="attraction__metres-mobile">60 км</span>
								<div class="attraction__divider"></div>
								<div class="attraction__item-icons">
									<img src="img/car.svg" alt="car">
									<img src="img/train.svg" alt="train">
									<img src="img/bus.png" alt="bus">
								</div>
							</div>
						</div>
						<div class="attraction__item">
							<img class="attraction__main-img" src="img/attraction-4.jpg" alt="attraction-item">
							<h4 class="attraction__item-title">Адлер</h4>
							<p>Олимпийский парк и спортивные объекты</p>
							<span>98 км</span>
							<div class="attraction__item-panel">
								<span class="attraction__metres-mobile">98 км</span>
								<div class="attraction__divider"></div>
								<div class="attraction__item-icons">
									<img src="img/car.svg" alt="car">
									<img src="img/train.svg" alt="train">
								</div>
							</div>
						</div>
						<div class="attraction__item">
							<img class="attraction__main-img" src="img/attraction-5.jpg" alt="attraction-item">
							<h4 class="attraction__item-title">Красная Поляна</h4>
							<p>Горы, лыжи, сноуборд</p>
						</div>
					</div>
				</div>
				<div class="parallax-wrap">
					<div class="parallax-item  layer" data-depth="0.8">
						<img src="img/leaf-4.png" alt="leaf">
					</div>
					<div class="parallax-item  layer" data-depth="0.15">
						<img src="img/leaf-5.png" alt="leaf">
					</div>
				</div>
			</div>
		</section>

		<section class="standarts" id="preimushchestva">
			<div class="container">
				<div class="main-title">
					<h2>Стандарты комфорта,<br>как в клубных апартаментах премиум-класса</h2>
				</div>
				<div class="standarts__list">
					<div class="standarts__item">
						<div class="standarts__image">
							<img src="img/standart-1.png" alt="standart-image">
						</div>
						<strong>Двор без машин</strong>
						<p>Территория комплекса<br>безопасна для всех. Парковки и подъездные дороги выведены<br>за пределы двора</p>
					</div>
					<div class="standarts__item">
						<div class="standarts__image">
							<img src="img/standart-3.png" alt="standart-image">
						</div>
						<strong>Охраняемая территория</strong>
						<p>Круглосуточная охрана, территория комплекса закрыта<br>для посторонних</p>
					</div>
					<div class="standarts__item">
						<div class="standarts__image">
							<img src="img/standart-2.png" alt="standart-image">
						</div>
						<strong>Бассейн во дворе</strong>
						<p>Бассейн бесплатно доступен<br>для всех жильцов комплекса</p>
					</div>
					<div class="standarts__item">
						<div class="standarts__image">
							<img src="img/standart-4.png" alt="standart-image">
						</div>
						<strong>Подземный паркинг</strong>
						<p>на 48 машиномест.<br>Придомовой паркинг на 50 машиномест</p>
					</div>
					<div class="standarts__item">
						<div class="standarts__image">
							<img src="img/standart-5.png" alt="standart-image">
						</div>
						<strong>Детская площадка</strong>
						<p>Построена по современным технологиям. Ваш ребенок полюбит игровую зону</p>
					</div>
					<div class="standarts__item">
						<div class="standarts__image">
							<img src="img/standart-6.png" alt="standart-image">
						</div>
						<strong>Зоны отдыха</strong>
						<p>Аллеи для прогулок в тени деревьев, лобби-бар, кафе и студия красоты будут работать на территории комплекса</p>
					</div>
				</div>
				<div class="parallax-wrap">
					<div class="parallax-item  layer" data-depth="1.2">
						<img src="img/s-leaf-1.png" alt="leaf">
					</div>
					<div class="parallax-item  layer" data-depth="0.15">
						<img src="img/s-leaf-2.png" alt="leaf">
					</div>
					<div class="parallax-item  layer" data-depth="0.7">
						<img src="img/s-leaf-3.png" alt="leaf">
					</div>
					<div class="parallax-item  layer" data-depth="0.7">
						<img src="img/s-leaf-4.png" alt="leaf">
					</div>
					<div class="parallax-item  layer" data-depth="0.15">
						<img src="img/s-leaf-5.png" alt="leaf">
					</div>
					<div class="parallax-item  layer" data-depth="0.6">
						<img src="img/s-leaf-6.png" alt="leaf">
					</div>
					<div class="parallax-item  layer" data-depth="0.5">
						<img src="img/s-leaf-7.png" alt="leaf">
					</div>
					<div class="parallax-item  layer" data-depth="0.15">
						<img src="img/s-leaf-8.png" alt="leaf">
					</div>
					<div class="parallax-item  layer" data-depth="3">
						<img src="img/s-leaf-9.png" alt="leaf">
					</div>
				</div>
			</div>
		</section>

		<section class="complex" style="background-image: url(img/complex-bg.jpg);">
			<div class="container">
				<div class="main-title">
					<h2>ЖК «Морской квартал» — это 6 трехэтажных корпусов</h2>
				</div>
				<div class="complex__list">
					<div class="complex__item">
						<p class="complex__item-title">Литер А</p>
					</div>
					<div class="complex__item">
						<p class="complex__item-title">Литер Б</p>
					</div>
					<div class="complex__item">
						<p class="complex__item-title">Литер В</p>
					</div>
					<div class="complex__item">
						<p class="complex__item-title">Литер Г</p>
					</div>
					<div class="complex__item">
						<p class="complex__item-title">Литер Д</p>
					</div>
					<div class="complex__item">
						<p class="complex__item-title">Литер Е</p>
					</div>
				</div>
			</div>
		</section>

		<section class="materials">
			<div class="container">
				<div class="main-title">
					<h2>При строительстве использованы экологичные и безопасные материалы</h2>
				</div>
				<div class="materials__list">
					<div class="materials__item">
						<div class="materials__item-container">
							<div class="materials__img">
								<img src="img/material-1.jpg" alt="material">
							</div>
							<strong>Декоративная<br>теплая штукатурка</strong>
						</div>
						<!-- <div class="materials__item-text">
							<p>Прекрасно сохраняет тепло, так что в вашей квартире комфортная температура будет сохраняться даже в холодные ветреные дни</p>
							<p>Не содержит вредных веществ, как другие фасадные материалы</p>
						</div> -->
					</div>
					<div class="materials__item">
						<div class="materials__item-container">
							<div class="materials__img">
								<img src="img/material-2.jpg" alt="material">
							</div>
							<strong>Клинкерный кирпич</strong>
						</div>
						<!-- <div class="materials__item-text">
							<p>Прекрасно сохраняет тепло, так что в вашей квартире комфортная температура будет сохраняться даже в холодные ветреные дни</p>
							<p>Не содержит вредных веществ, как другие фасадные материалы</p>
						</div> -->
					</div>
					<div class="materials__item">
						<div class="materials__item-container">
							<div class="materials__img">
								<img src="img/material-3.jpg" alt="material">
							</div>
							<strong>Керамогранит</strong>
						</div>
						<!-- <div class="materials__item-text">
							<p>Прекрасно сохраняет тепло, так что в вашей квартире комфортная температура будет сохраняться даже в холодные ветреные дни</p>
							<p>Не содержит вредных веществ, как другие фасадные материалы</p>
							<p>Прекрасно сохраняет тепло, так что в вашей квартире комфортная температура будет сохраняться даже в холодные ветреные дни</p>
						</div> -->
					</div>
					<div class="materials__item">
						<div class="materials__item-container">
							<div class="materials__img">
								<img src="img/material-4.jpg" alt="material">
							</div>
							<strong>Монолитно-каркасная технология</strong>
						</div>
						<!-- <div class="materials__item-text">
							<p>Прекрасно сохраняет тепло, так что в вашей квартире комфортная температура будет сохраняться даже в холодные ветреные дни</p>
						</div> -->
					</div>
				</div>
				<div class="materials__panel">
					<a data-fancybox data-src="#popup" href="javascript://" class="btn-main  btn-main--col"><span>скачать подробное<br>описание проекта</span></a>
				</div>
			</div>
		</section>

		<section class="review" id="prezentatsiya" style="background-image: url(img/review-bg.jpg);">
			<div class="container">
				<div class="review__content">
					<div class="main-title">
						<h2>Приезжайте<br>на презентацию<br>ЖК «Морской квартал»<br>в Сочи бесплатно</h2>
						<p>Мы оплатим вам перелет и номер в гостинице в случае сделки</p>
					</div>
					<ul>
						<li>Вы побываете на стройплощадке</li>
						<li>Своими глазами увидите вашу будущую квартиру</li>
						<li>Пройдетесь по территории и от жилого комплекса до пляжа и инфраструктурных объектов</li>
						<li>Зададите интересующие вас вопросы напрямую застройщику</li>
					</ul>
				</div>
				<div class="main-form">
					<div class="main-form__wrapper">
						<strong class="main-form__title">Введите ваши данные</strong>
						<p class="main-form__subtitle">чтобы оставить заявку <br>на презентацию объекта в Сочи</p>
						<form action="" class="name-tel-form" novalidate="novalidate">
							<input type="text" name="name" placeholder="Введите ваше имя">
							<input type="tel" name="tel" placeholder="Введите ваш телефон">
							<div class="main-form__checkbox">
								<input type="checkbox" name="amount" id="comming" checked value="Хочу приехать не один">
								<label for="comming">Хочу приехать не один</label>
							</div>
							<button class="btn-main  btn-main--col"><span>хочу приехать<br>на презентацию</span></button>
							<input type="hidden" name="form_name" value="Заявка на презентацию объекта в Сочи">
							<input type="hidden" name="utm" value="<?php echo htmlspecialchars(json_encode($_GET)); ?>">
						</form>
						<p class="main-form__description">Нажимая на кнопку, вы принимаете <br>условия <a href="#">передачи данных</a></p>
					</div>
				</div>
				<div class="parallax-wrap">
					<div class="parallax-item  layer" data-depth="0.05">
						<img src="img/plane.png" alt="plane">
					</div>
					<div class="parallax-item  layer" data-depth="0.8">
						<img src="img/r-leaf.png" alt="leaf">
					</div>
				</div>
			</div>
		</section>

		<section class="reputation">
			<div class="container">
				<div class="main-title">
					<h2>Надежный застройщик AR Group с кристальной репутацией</h2>
					<p>Гарантирует исполнение своих обязательств качественно и в срок</p>
				</div>
				<strong class="reputation__title">Объекты, сданные AR Group</strong>
				<div class="reputation__list">
					<div class="reputation__item">
						<div class="reputation__item-wrap">
							<div class="reputation__item-info">
								<div class="reputation__image">
									<img src="img/house-1.jpg" alt="houses">
								</div>
								<strong>ЖК «Белый дворец»</strong>
								<span class="reputation__name">Сочи</span>
								<p>Жилой высотный комплекс премиум-класса на 750 квартир и 2-уровневой подземной парковкой</p>
							</div>
							<a href="http://www.white-palace.ru/" target="_blank">www.white-palace.ru</a>
						</div>
					</div>
					<div class="reputation__item">
						<div class="reputation__item-wrap">
							<div class="reputation__item-info">
								<div class="reputation__image">
									<img src="img/house-2.jpg" alt="houses">
								</div>
								<strong>ЖК «Морская симфония»</strong>
								<span class="reputation__name">Сочи</span>
								<p>Жилой комплекс<br>повышенной комфортности<br>на 240 квартир</p>
							</div>
							<a href="http://www.morsim.ru/" target="_blank">www.morsim.ru</a>
						</div>
					</div>
					<div class="reputation__item">
						<div class="reputation__item-wrap">
							<div class="reputation__item-info">
								<div class="reputation__image">
									<img src="img/house-3.jpg" alt="houses">
								</div>
								<strong>АК «Лучезарный»</strong>
								<span class="reputation__name">Сочи</span>
								<p>Элитный 7-этажный<br>апарт-комплекс на берегу Черного моря с бассейном и собственным парком 3,2 Га</p>
							</div>
							<a href="http://luchezarniy.ru/" target="_blank">www.luchezarniy.ru</a>
						</div>
					</div>
				</div>
				<div class="parallax-wrap">
					<div class="parallax-item  layer" data-depth="0.4">
						<img src="img/rep-leaf.png" alt="leaf">
					</div>
				</div>
			</div>
		</section>

		<section class="deal" id="sdelka">
			<div class="container">
				<div class="main-title">
					<h2>Законность и прозрачность сделки регламентированы ФЗ-215</h2>
				</div>
				<div class="deal__list">
					<div class="deal__item">
						<div class="deal__image">
							<img src="img/doc-1.png" alt="doc">
						</div>
						<div class="deal__item-text">
							<strong>Выписка из ЕГРН</strong>
							<p>Из документа вы узнаете сведения об объекте недвижимости, зарегистрированные права на объект и убедитесь, что объект юридически чист</p>
						</div>
					</div>
					<div class="main-form">
						<div class="main-form__wrapper">
							<strong class="main-form__title">Получите весь пакет документов для скачивания</strong>
							<p class="main-form__subtitle">Включая договор купли-продажи и проектную декларацию</p>
							<form action="" class="mail-tel-form" novalidate="novalidate">
								<input type="tel" name="tel" placeholder="Введите ваш телефон">
								<input type="email" name="email" placeholder="Введите вашу почту">
								<button class="btn-main"><span>скачать пакет документов</span></button>
								<input type="hidden" name="form_name" value="Получить весь пакет документов для скачивания">
								<input type="hidden" name="utm" value="<?php echo htmlspecialchars(json_encode($_GET)); ?>">
							</form>
						</div>
					</div>
					<div class="deal__item">
						<div class="deal__image">
							<img src="img/doc-2.png" alt="doc">
						</div>
						<div class="deal__item-text">
							<strong>Разрешение<br>на строительство</strong>
							<p>Убедитесь, что застройщик имеет законное право осуществлять строительство</p>
						</div>
					</div>
				</div>
			</div>
			<div class="parallax-wrap">
				<div class="parallax-item  layer" data-depth="0.05">
					<img src="img/deal-tree-1.png" alt="trees">
				</div>
				<div class="parallax-item  layer" data-depth="0.12">
					<img src="img/deal-tree-2.png" alt="trees">
				</div>
			</div>
		</section>

		<section class="payment" id="rassrochka" style="background-image: url(img/paymant-bg.jpg);">
			<div class="container">
				<div class="payment__content">
					<div class="main-title">
						<h2>Возможна <br>рассрочка на 3 месяца при первом взносе 70%</h2>
						<p>Удобный график и выгодное <br>распределение платежей</p>
					</div>
					<div class="payment__info">
						<img src="img/pay-doc.png" alt="pay-doc">
						<p>Менеджер отдела продаж свяжется с вами, назовет точную стоимость квартиры, рассчитает первый платеж и составит график выплат</p>
					</div>
				</div>
				<div class="main-form">
					<div class="main-form__wrapper">
						<strong class="main-form__title">Оставьте заявку</strong>
						<p class="main-form__subtitle">и получите индивидуальные <br>условия рассрочки</p>
						<form action="" class="name-tel-form" novalidate="novalidate">
							<input type="text" name="name" placeholder="Введите ваше имя">
							<input type="tel" name="tel" placeholder="Введите ваш телефон">
							<button class="btn-main  btn-main--col"><span>получить<br>индивидуальные условия</span></button>
							<input type="hidden" name="form_name" value="Получить индивидуальные условия рассрочки">
							<input type="hidden" name="utm" value="<?php echo htmlspecialchars(json_encode($_GET)); ?>">
						</form>
						<p class="main-form__description">Нажимая на кнопку, вы принимаете <br>условия <a href="#">передачи данных</a></p>
					</div>
				</div>
				<img src="img/girl.png" alt="girl" class="girl">
			</div>
			<div class="parallax-wrap">
				<div class="parallax-item  layer" data-depth="0.07">
					<img src="img/bush.png" alt="bush">
				</div>
			</div>
		</section>

		<section class="map-block">
			<div class="container">
				<div class="main-title">
					<h2>Остались вопросы?</h2>
					<p>Приезжайте в офис отдела продаж</p>
				</div>
				<div class="map-block__wrap">
					<div id="map"></div>
					<div class="map-block__content">
						<div class="map-block__content-wrap">
							<div class="map-block__item">
								<strong>адрес</strong>
								<p>Сочи, пос. Вардане, <br>ул. Львовская, 92</p>
							</div>
							<div class="map-block__item">
								<strong>Режим работы</strong>
								<p>Пн-пт 10.00-18.00</p>
								<p>Сб 10.00-15.00</p>
							</div>
							<div class="map-block__item">
								<strong>телефон</strong>
								<a href="tel:+79683002066" class="jsTrackContact">+7 (968) 300-20-66</a>
							</div>
							<div class="map-block__item">
								<strong>Email</strong>
								<a href="mailto:sales@morskoikvartal.ru" class="jsTrackContact">sales@morskoikvartal.ru</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<img src="img/palm.png" alt="palm" class="map-block__palm">
		</section>

		<footer class="footer">
			<div class="container">
				<div class="footer__info">
					<div class="footer__logo-content">
						<a href="index.html" class="footer__logo"><img src="img/logo.png" alt="logo"></a>
						<div class="footer__text">
							<strong>«Морской квартал» —</strong>
							<p>позвольте себе квартиру <br>на берегу Черного моря</p>
						</div>
					</div>
					<nav class="footer__menu">
						<ul>
							<li><a href="#">Проектные декларации</a></li>
							<li><a href="#">Разрешительная документация</a></li>
							<li><a href="#">Политика конфиденциальности</a></li>
						</ul>
					</nav>
				</div>
				<div class="footer__contacts">
					<!-- <div class="footer__socials">
						<a href="#" class="footer__social"><img src="img/vk.svg" alt="vk"></a>
						<a href="#" class="footer__social  facebook"><img src="img/facebook.svg" alt="facebook"></a>
						<a href="#" class="footer__social  instagram"><img src="img/instagram.svg" alt="instagram"></a>
					</div> -->
					<div class="footer__contacts-content">
						<a href="tel:+79683002066" class="footer__tel jsTrackContact">+7 968 300-20-66</a>
						<a href="mailto:sales@morskoikvartal.ru" class="footer__mail jsTrackContact">sales@morskoikvartal.ru</a>
					</div>
				</div>
			</div>
		</footer>
	</div>

	<div class="popup main-form" id="popup">
		<div class="main-form__wrapper">
			<strong class="main-form__title">Введите ваш номер телефона,</strong>
			<p class="main-form__subtitle">И специалист отдела продаж<br>позвонит вам сейчас или в удобное для звонка время</p>
			<form action="" class="tel-form" novalidate="novalidate">
				<input type="tel" name="tel" placeholder="Введите ваш телефон">
				<button class="btn-main"><span>позвоните мне</span></button>
				<input type="hidden" name="form_name" value="Звонок клиенту">
				<input type="hidden" name="utm" value="<?php echo htmlspecialchars(json_encode($_GET)); ?>">
			</form>
			<p class="main-form__description">Нажимая на кнопку, вы принимаете <br>условия <a href="#">передачи данных</a></p>
		</div>
	</div>

	<div class="popup  popup-big  main-form" id="popup-2">
		<div class="main-form__wrapper">
			<div class="popup__image-wrap" style="background-image: url(img/popup-2-bg.jpg);"></div>
			<div class="popup__main">
				<strong class="main-form__title">Узнайте наличие квартир в ЖК<br>«Морской квартал»</strong>
				<p class="main-form__subtitle">Заполните поля ниже — и менеджер<br>свяжется с вами и сообщит, какие варианты доступны для бронирования</p>
				<form action="" class="name-tel-form" novalidate="novalidate">
					<input type="text" name="name" placeholder="Введите ваше имя">
					<input type="tel" name="tel" placeholder="Введите ваш телефон">
					<button class="btn-main"><span>узнать наличие</span></button>
					<input type="hidden" name="form_name" value="Наличие квартир в ЖК «Морской квартал»">
					<input type="hidden" name="utm" value="<?php echo htmlspecialchars(json_encode($_GET)); ?>">
				</form>
				<p class="main-form__description">Нажимая на кнопку, вы принимаете <br>условия <a href="#">передачи данных</a></p>
			</div>
		</div>
	</div>

	<div class="popup  popup-big  main-form" id="popup-3">
		<div class="main-form__wrapper">
			<div class="popup__image-wrap" style="background-image: url(img/popup-3-bg.jpg);"></div>
			<div class="popup__main">
				<strong class="main-form__title">Подберите<br>квартиру в ЖК<br>«Морской квартал»</strong>
				<p class="main-form__subtitle">Заполните поля ниже — и менеджер свяжется с вами и подберет для вас несколько доступных для бронирования вариантов</p>
				<form action="" class="name-tel-form" novalidate="novalidate">
					<input type="text" name="name" placeholder="Введите ваше имя">
					<input type="tel" name="tel" placeholder="Введите ваш телефон">
					<button class="btn-main"><span>подобрать квартиру</span></button>
					<input type="hidden" name="form_name" value="Подобрать квартиру в ЖК «Морской квартал»">
					<input type="hidden" name="utm" value="<?php echo htmlspecialchars(json_encode($_GET)); ?>">
				</form>
				<p class="main-form__description">Нажимая на кнопку, вы принимаете <br>условия <a href="#">передачи данных</a></p>
			</div>
		</div>
	</div>
	
	<script src="js/vendor.js"></script>
	<script src="js/main.js"></script>
	<script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAhjCQNBCef_6oHiYOGRdVRAJRz8fNfT6Q&amp;callback=initMap"></script>
</body>
</html>