var map;
function initMap() {
 map = new google.maps.Map(document.getElementById('map'), {
  center: {lat: 43.7292306, lng: 39.5576753},
   zoom: 19,
   scrollwheel: false,
   zoomControl: false,
   disableDefaultUI: true, 
   styles: [
     
     {elementType: 'labels.text.stroke', stylers: [{color: '#ffffff'}]},
     {elementType: 'labels.text.fill', stylers: [{color: '#84898b'}]},
     {
       featureType: 'administrative.locality',
       elementType: 'labels.text.fill',
       stylers: [{color: '#84898b'}]
     },
     {
       featureType: 'poi',
       elementType: 'labels.text.fill',
       stylers: [{color: '#84898b'}]
     },
     {
       featureType: 'poi.park',
       elementType: 'labels.text.stroke',
       stylers: [{color: '#ffffff'}]
     },
     {
       featureType: 'road',
       elementType: 'geometry',
       stylers: [{color: '#ffffff'}]
     },
     {
       featureType: 'road',
       elementType: 'geometry.stroke',
       stylers: [{color: '#e5e5e5'}]
     },
     {
       featureType: 'road',
       elementType: 'labels.text.fill',
       stylers: [{color: '#84898b'}]
     },
     {
       featureType: 'road.highway',
       elementType: 'geometry',
       stylers: [{color: '#d8d8d8'}]
     },
     {
       featureType: 'road.highway',
       elementType: 'geometry.stroke',
       stylers: [{color: '#acacac'}]
     },
     {
       featureType: 'road.highway',
       elementType: 'labels.text.fill',
       stylers: [{color: '#414141'}]
     },
     {
       featureType: 'transit',
       elementType: 'geometry',
       stylers: [{color: '#ffffff'}]
     },
     {
       featureType: 'transit.station',
       elementType: 'labels.text.fill',
       stylers: [{color: '#ffffff'}]
     }
   ]
 });

 var marker;
  marker = new google.maps.Marker({

   position: {lat: 43.7292306, lng: 39.5576753},          
   map: map,                           
   icon:"img/map-marker.png"
 });


};



$(function () {


	if ($(window).width() > 1023) {

	  $('.parallax-wrap').parallax({
	    calibrateX: true,
	    calibrateY: false,
	    limitX: false,
	    limitY: false,
	    scalarX: 2,
	    scalarY: 10,
	    frictionX: 0.2,
	    frictionY: 0.2
	  });

	}



   $('.mail-tel-form').each(function(i, item) {
     var $form = $(item);

     $form.validate({
       rules: {
           tel: {
               required: true
           },
           email: {
               required: true
           }

       },
       messages: {
           tel: {
               required: 'Укажите ваш телефон'
           },
           email: {
               required: 'Укажите вашу почту'
           }
       },
       submitHandler: function(form) {
           var $form = $(form);
           $.ajax({
               type: "POST",
               url: "mail-tel-fields.php",
               data: $form.serialize()
           }).done(function(e) {
               $form.find('.dispatch-message').removeClass('error').addClass('success').html('Спасибо! Мы свяжемся с вами').slideDown();
               $form.trigger('reset');
               window.location = "thanks-page.html"
           }).fail(function (e) {
               $form.find('.dispatch-message').removeClass('success').addClass('error').html('Произошла ошибка. Попробуйте еще раз').slideDown();
               $form.trigger('reset');
               setTimeout(function () {
                   $form.find('.dispatch-message').slideUp();
               }, 5000);
           })
       }
     });

   });

  $('.name-tel-form').each(function(i, item) {
     var $form = $(item);

     $form.validate({
       rules: {
           tel: {
               required: true
           },
           name: {
               required: true
           }

       },
       messages: {
           tel: {
               required: 'Укажите ваш телефон'
           },
           name: {
               required: 'Укажите ваше имя'
           }
       },
       submitHandler: function(form) {
           var $form = $(form);
           $.ajax({
               type: "POST",
               url: "name-tel-fields.php",
               data: $form.serialize()
           }).done(function(e) {
               $form.find('.dispatch-message').removeClass('error').addClass('success').html('Спасибо! Мы свяжемся с вами').slideDown();
               $form.trigger('reset');
               window.location = "thanks-page.html"
           }).fail(function (e) {
               $form.find('.dispatch-message').removeClass('success').addClass('error').html('Произошла ошибка. Попробуйте еще раз').slideDown();
               $form.trigger('reset');
               setTimeout(function () {
                   $form.find('.dispatch-message').slideUp();
               }, 5000);
           })
       }
     });

   }); 

  $('.email-form').each(function(i, item) {
     var $form = $(item);

     $form.validate({
       rules: {
           email: {
               required: true
           }

       },
       messages: {
           email: {
               required: 'Укажите вашу почту'
           }
       },
       submitHandler: function(form) {
           var $form = $(form);
           $.ajax({
               type: "POST",
               url: "email-fields.php",
               data: $form.serialize()
           }).done(function(e) {
               $form.find('.dispatch-message').removeClass('error').addClass('success').html('Спасибо! Мы свяжемся с вами').slideDown();
               $form.trigger('reset');
               window.location = "thanks-page.html"
           }).fail(function (e) {
               $form.find('.dispatch-message').removeClass('success').addClass('error').html('Произошла ошибка. Попробуйте еще раз').slideDown();
               $form.trigger('reset');
               setTimeout(function () {
                   $form.find('.dispatch-message').slideUp();
               }, 5000);
           })
       }
     });

   }); 

  $('.tel-form').each(function(i, item) {
     var $form = $(item);

     $form.validate({
       rules: {
           tel: {
               required: true
           }

       },
       messages: {
           tel: {
               required: 'Укажите ваш телефон'
           }
       },
       submitHandler: function(form) {
           var $form = $(form);
           $.ajax({
               type: "POST",
               url: "tel-fields.php",
               data: $form.serialize()
           }).done(function(e) {
               $form.find('.dispatch-message').removeClass('error').addClass('success').html('Спасибо! Мы свяжемся с вами').slideDown();
               $form.trigger('reset');
               window.location = "thanks-page.html"
           }).fail(function (e) {
               $form.find('.dispatch-message').removeClass('success').addClass('error').html('Произошла ошибка. Попробуйте еще раз').slideDown();
               $form.trigger('reset');
               setTimeout(function () {
                   $form.find('.dispatch-message').slideUp();
               }, 5000);
           })
       }
     });

   }); 


  $('input[name="tel"]').inputmask({mask: "+7 (999) 9999999"});


    $("[data-fancybox]").fancybox({
        afterShow: function( instance, slide ) {
            var slideType = slide.type;
            if (slideType === 'inline') {
                fbq('track', 'FormOpen_mkvartal', {
                    value: 2
                });
            }
        }
    });
    $('.jsTrackContact').on('click', function () {
        fbq('track', 'Contact_mkvartal', {
            value: 4
        });
    });

});